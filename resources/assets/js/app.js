import CreateGroup from "./components/CreateGroup";


require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import VueAxios from 'vue-axios';
import axios from 'axios';

import App from './App.vue';
Vue.use(VueAxios, axios);

import HomeComponent from './components/ExampleComponent.vue';
import CreateUser from './components/CreateUser.vue';
import CreateGroupComponent from './components/CreateGroup.vue';
import PhonesEdit from './components/PhonesEdit.vue';
import GroupsEdit from './components/GroupsEdit.vue';
import UsingComponent from './components/UsingComponent';

import FilteringUsingComponent from './components/FilteringUsingComponent';

window.Vue.component('create-user',CreateUser);
window.Vue.component('create-group',CreateGroupComponent);
window.Vue.component('using',FilteringUsingComponent);

import HuxTable from './components/common/HuxTable.vue';
window.Vue.component('hux-table',HuxTable);

import VPaginator from './components/paginator/VPaginator.vue';
window.Vue.component('v-paginator',VPaginator);

import Vue from 'vue'
import Vuex from 'vuex'

require('bootstrap-select');

Vue.use(Vuex)

import ulakBELStore from './stores/1550150190_ulakBELStore';



const routes = [
    {
        name: 'home',
        path: '/',
        component: HomeComponent,
    },
    {
        name: 'using',
        path: '/using',
        component: UsingComponent,
    },
    {
        name: 'create',
        path: 'create',
        component: CreateUser
    },
    {
        name: 'PhonesEdit',
        path: '/edit/:id',
        component: PhonesEdit
    },
    {
        name: 'creategroup',
        path: '/creategroup',
        component: CreateGroupComponent
    },
    {
        name: 'GroupsEdit',
        path: '/editgroup/:id',
        component: GroupsEdit
    },
    {
        name: 'GroupsEdit',
        path: '/editgroup/:id',
        component: GroupsEdit
    }

];



const router = new VueRouter({ routes: routes});

const app = new Vue(
    {
        router,
        store: ulakBELStore,
        data: function () {
            return {
                locales:
                    {
                        'table_headers_Phones_name': 'İsim',
                        'table_headers_Phones_phone_number': 'Telefon No',
                        'table_headers_Phones_email': 'E-Mail',
                        'table_headers_Phones_fax_number': 'Faks Numarası',
                        'table_headers_Phones_include_sms_list': 'Sms Listesi Mi?',
                        'table_headers_Phones_include_fax_list': 'Faks Listesinde Mi?',
                        'table_headers_Phones_include_email_list': 'E-Mail Listesinde Mi?',
                        'table_headers_Phones_active': 'Aktif Mi?',
                        'table_headers_Phones_group_id': 'Grubu',
                        'table_headers_Groups_name': 'İsim',
                        'table_headers_Groups_active': 'Aktif Mi?',

                    }
            }

        }
    }).$mount('#app');
