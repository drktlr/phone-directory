import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

const state = {
    stateModel: {},
    CC:{
        phone_number:false,
        seconds:0,
        uid:false,
        model_incident_create:{},
        contact:{id:-1},
    },
    _refresh:{
        IncidentIndex:0,
    },
    searchBulkKey: '',
    _search_texts:{},
    _sort:{},
    _huxTable:{},
}

const getters = {
    welcomeMessage(state) {
        return `${state.message} ${state.username}`;
    },
}

const mutations = {
    setUserName(state,name) {
        state.username = name;
    },
    setContact(state,contact) {
        state.contact = contact;
    },
    setModel(state,model) {
        state.stateModel[model.key] = model.value;
    },
    setCC(state,data) {
        state.CC[data.key] = data.value;
    },
    setRefresh(state,key) {
        state._refresh[key]++;
    },
    setSearchBulkKey(state,key) {
        state.searchBulkKey = key;
    },
    setSearchTexts(state,data) {
        state._search_texts[data.key] = data.value;
    },
    setSort(state,data) {
        state._sort[data.key] = data.value;
    },
    setHuxTableStates(state,data) {
        state._huxTable[data.key] = data.value;
    },
    resetStates(state) {

        state.stateModel = {};
        state.CC = {
            phone_number:false,
            seconds:0,
            uid:false,
            model_incident_create:{},
            contact:{id:-1},
        };
        state._refresh = {
            IncidentIndex:0,
        };
        state.searchBulkKey = '';
        state._search_texts = {};
        state._sort = {};
        state._huxTable = {};

    },
}

const actions = {
    updateUsername({commit},name) {
        commit('setUserName',name);
    },
    updateContact({commit},contact) {
        commit('setContact',contact);
    },
    updateModel({commit},model) {
        commit('setModel',model);
    },
    updateCC({commit},data) {
        commit('setCC',data);
    },
    updateRefresh({commit},key) {
        commit('setRefresh',key);
    },
    updateSearchBulkKey({commit},key) {
        commit('setSearchBulkKey',key);
    },
    updateSearchTexts({commit},data) {
        commit('setSearchTexts',data);
    },
    updateSort({commit},data) {
        commit('setSort',data);
    },
    updateHuxTableStates({commit},data) {
        commit('setHuxTableStates',data);
    },
    resetStates({commit}) {
        commit('resetStates');
    },
}

const ulakBELStore = new Vuex.Store({
    state,
    getters,
    mutations,
    actions
})

export default ulakBELStore;