<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhoneDirectoryPhonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phone_directory_phones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->unsignedBigInteger('phone_number');
            $table->string('email');
            $table->unsignedBigInteger('fax_number');
            $table->boolean('include_sms_list');
            $table->boolean('include_fax_list');
            $table->boolean('include_email_list');
            $table->boolean('active');
            $table->integer('group_id');
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phone_directory_phones');
    }
}
