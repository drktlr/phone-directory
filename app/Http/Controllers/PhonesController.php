<?php

namespace App\Http\Controllers;

use App\Http\Resources\PhoneCollection;
use App\PhoneDirectoryGroups;
use Illuminate\Http\Request;
use App\PhoneDirectoryPhones;

class PhonesController extends Controller
{
    public $timestamps = false;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        return PhoneDirectoryPhones::paginate( intval($request->count)?:10 );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|min:3',
            'phone_number' => 'regex:/(5)[0-9]{9}/',
            'email' => 'required|email',
            'fax_number' => 'regex:/^[^5][0-9]{10}/'
        ]);



        $contact = new PhoneDirectoryPhones([
            'name' => $request->input('name'),
            'phone_number' => $request->input('phone'),
            'email' => $request->input('email'),
            'fax_number' => $request->input('fax'),
            'include_sms_list' =>intval($request->input('sms_list')),
            'include_fax_list' => intval($request->input('fax_list')),
            'include_email_list' =>intval($request->input('email_list')),
            'active' => intval($request->input('active')),
            'group_id' => intval($request->input('group_id'))
        ]);

        $contact->save();

        return response()->json('successfully added');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function getPhones($id)
    {
        //
    }

    public function getTypes(Request $request, $type)
    {
        if($type === 'sms')
        {
            $types = PhoneDirectoryGroups::with('phones')->where('active', 1)->whereHas('phones', function ($query) {
                $query->where('include_sms_list', 1)->where('active',1);
            })->get();

        }
        else if($type === 'email')
        {
            $types = PhoneDirectoryGroups::with('phones')->where('active', 1)->whereHas('phones', function ($query) {
                $query->where('include_email_list', 1)->where('active',1);
            })->get();


        }
        else
        {
            $types = PhoneDirectoryGroups::with('phones')->where('active', 1)->whereHas('phones', function ($query) {
                $query->where('include_fax_list', 1)->where('active',1);
            })->get();

        }

        return response()->json($types);
    }

    public function findFaxNames($id)
    {
        $names = PhoneDirectoryPhones::where('id', $id)->where('include_fax_list', 1)->where('active', 1)->get();
        return response()->json($names);

    }
    public function findSmsNames($id)
    {
        $names = PhoneDirectoryPhones::where('id', $id)->where('include_sms_list', 1)->where('active', 1)->get();
        return response()->json($names);

    }
    public function findEmailNames($id)
    {
        $names = PhoneDirectoryPhones::where('id', $id)->where('include_email_list', 1)->where('active', 1)->get();
        return response()->json($names);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = PhoneDirectoryPhones::find($id);
        return response()->json($user);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = PhoneDirectoryPhones::find($id);

        $user->update($request->all());

        return response()->json('successfully updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $phone = PhoneDirectoryPhones::find($id);

        $phone->delete();

        return response()->json('successfully deleted');

    }




}
