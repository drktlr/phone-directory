<?php

namespace App\Http\Controllers;

use App\Http\Resources\PhoneGroupCollection;
use Illuminate\Http\Request;
use App\PhoneDirectoryGroups;

class PhonesGroupController extends Controller
{
    public $timestamps = false;

    public function create(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|min:2',
        ]);


        $group = new PhoneDirectoryGroups([
            'name' => $request->input('name'),
            'active' => intval($request->input('active'))
        ]);

        $group->save();

        return response()->json('successfully added');
    }

    public function index(Request $request)
    {
        return PhoneDirectoryGroups::paginate( intval($request->count)?:10 );

    }

    public function getPosts()
    {
        return PhoneDirectoryGroups::all();

    }

    public function getGroupName($id)
    {

        return PhoneDirectoryGroups::where('id', $id)->where('active', 1)->get();

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $group = PhoneDirectoryGroups::find($id);
        return response()->json($group);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $group = PhoneDirectoryGroups::find($id);

        $group->update($request->all());

        return response()->json('successfully updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $group = PhoneDirectoryGroups::find($id);

        $group->delete();

        return response()->json('successfully deleted');

    }

}
