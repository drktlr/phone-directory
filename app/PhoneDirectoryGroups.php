<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhoneDirectoryGroups extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name', 'active', 'deleted_at'
    ];


    public function phones()
    {
        return $this->hasMany('App\PhoneDirectoryPhones','group_id','id');
    }
}
