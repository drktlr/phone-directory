<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PhoneDirectoryPhones extends Model
{
    use SoftDeletes;
    public $timestamps = false;

    protected $fillable = [
        'name', 'phone_number', 'email', 'fax_number', 'include_sms_list', 'include_fax_list', 'include_email_list', 'active','group_id', 'deleted_at'
    ];

    public function group()
    {
        return $this->belongsTo('App\PhoneDirectoryGroups', 'group_id');
    }

}
