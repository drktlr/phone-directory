<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('create', 'PhonesController@create');
Route::post('creategroup', 'PhonesGroupController@create');
Route::get('/edit/{id}', 'PhonesController@edit');
Route::post('/update/{id}', 'PhonesController@update');
Route::delete('/delete/{id}', 'PhonesController@destroy');
Route::get('/editgroup/{id}', 'PhonesGroupController@edit');
Route::post('/updategroup/{id}', 'PhonesGroupController@update');
Route::delete('/deletegroup/{id}', 'PhonesGroupController@destroy');
Route::get('/posts', 'PhonesController@index');
Route::get('/groupposts', 'PhonesGroupController@index');
Route::get('/grouppostsall', 'PhonesGroupController@getPosts');

Route::get('/grouppostsall/{type}', 'PhonesController@getTypes');
Route::get('/getphones/{id}', 'PhonesController@getPhones');


Route::get('/getSmsNames/{id}', 'PhonesController@findSmsNames');
Route::get('/getEmailNames/{id}', 'PhonesController@findEmailNames');
Route::get('/getFaxNames/{id}', 'PhonesController@findFaxNames');
